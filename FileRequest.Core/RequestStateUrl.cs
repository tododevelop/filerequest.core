﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileRequest.Core
{
    /// <summary>
    /// Объект для передачи состояния запроса между физическими уронями включает url файла
    /// </summary>
    [Serializable]
    public class RequestStateUrl: RequestStateBase
    {
        /// <summary>
        /// Результат запроса
        /// </summary>
        public List<FileUrlDto> Result { get; set; }
    }
}
