﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileRequest.Core
{
    /// <summary>
    /// Базовый класс для передачи состояния запроса между физическими уронями
    /// </summary>
    [Serializable]
    public abstract class RequestStateBase
    {
        /// <summary>
        /// Если ошибок не возникло - true
        /// </summary>
        public bool IsOk { get; set; }
        //public object Result { get; set; }
        /// <summary>
        /// если возникли ошибки - текстовое сообщение об ошибке
        /// </summary>
        public string ErrorMessage { get; set; } = "";
    }
}
