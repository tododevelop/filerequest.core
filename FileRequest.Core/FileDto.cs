﻿using System;

namespace FileRequest.Core
{
    /// <summary>
    /// Класс транспортных объектов для передачи файлов между физическими уровнями
    /// </summary>
    [Serializable]
    public class FileDto
    {
        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Размер файла
        /// </summary>
        public int Size { get; set; }
        /// <summary>
        /// Mime тип
        /// </summary>
        public string MimeType { get; set; }
        /// <summary>
        /// Тело файла
        /// </summary>
        public byte[] Body { get; set; }
    }
}
