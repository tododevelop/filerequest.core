﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileRequest.Core
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class FileUrlDto:FileDto
    {
        /// <summary>
        /// Ссылка на файл сохраненный на сервере
        /// </summary>
        public string ServerUrl { get; set; }
    }
}
