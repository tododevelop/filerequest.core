﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileRequest.Core
{
    /// <summary>
    /// Объект для передачи состояния запроса между физическими уронями
    /// </summary>
    [Serializable]
    public class RequestState: RequestStateBase
    {
        /// <summary>
        /// Результат запроса
        /// </summary>
        public List<FileDto> Result { get; set; }
    }
}
